import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appColorAnswer]'
})
export class ColorAnswerDirective {
  className = 'border';

  @Input() set appColorAnswer(status: string) {
    if(status === 'Ответ верный') {
      this.className = 'bg-success';
      this.renderer.addClass(this.el.nativeElement, this.className);
    } else if(status === 'Ответ неверный') {
      this.className = 'bg-danger';
      this.renderer.addClass(this.el.nativeElement, this.className);
    }
  }

  constructor(private el: ElementRef, private renderer: Renderer2) {}
}
