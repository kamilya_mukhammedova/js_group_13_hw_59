import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appShowClue]'
})
export class ShowDirective {
  isOpen = false;

  constructor(private el: ElementRef, private renderer: Renderer2) {}

  @HostListener('click') open() {
    this.isOpen = !this.isOpen;

    if(this.isOpen) {
      this.renderer.addClass(this.el.nativeElement, 'showClue');
      this.el.nativeElement.children[1].style.display = 'block';
    } else {
      this.renderer.removeClass(this.el.nativeElement, 'showClue');
      this.el.nativeElement.children[1].style.display = 'none';
    }
  }
}
