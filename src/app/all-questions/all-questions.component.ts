import { Component, OnInit } from '@angular/core';
import { Question } from '../shared/question.model';
import { QuestionService } from '../shared/question.service';

@Component({
  selector: 'app-all-questions',
  templateUrl: './all-questions.component.html',
  styleUrls: ['./all-questions.component.css']
})
export class AllQuestionsComponent implements OnInit {
  questionsArray: Question[] = [];
  correctAnswer = 0;

  constructor(private questionService: QuestionService) {}

  ngOnInit() {
    this.questionsArray = this.questionService.getQuestions();
    this.questionService.questionsArrayChange.subscribe((question: Question[]) => {
      this.questionsArray = question;
    });
  }

  getCorrectAnswers() {
    this.correctAnswer = 0;
    for(let i = 0; i < this.questionsArray.length; i++) {
      if(this.questionsArray[i].status === 'Ответ верный') {
        this.correctAnswer++;
      }
    }
    return this.correctAnswer;
  }
}
