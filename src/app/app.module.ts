import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AllQuestionsComponent } from './all-questions/all-questions.component';
import { QuestionComponent } from './question/question.component';
import { QuestionService } from './shared/question.service';
import { FormComponent } from './form/form.component';
import { FormsModule } from '@angular/forms';
import { ShowDirective } from './directives/show.directive';
import { ColorAnswerDirective } from './directives/color-answer.directive';

@NgModule({
  declarations: [
    AppComponent,
    AllQuestionsComponent,
    QuestionComponent,
    FormComponent,
    ShowDirective,
    ColorAnswerDirective
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [QuestionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
