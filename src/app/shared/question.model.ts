export class Question {
  status: string = 'Нет ответа';

  constructor
    (public text: string,
     public correctAnswer: string,
     public clue: string,
    ) {}
}
