import { Question } from './question.model';
import { EventEmitter } from '@angular/core';

export class QuestionService {
  questionsArrayChange = new EventEmitter<Question[]>();

  private questionsArray = [
    new Question('Он знаком многим. Был и художником, и доктором, и путешественником, и космонавтом. Кто это?', 'Незнайка', 'Персонаж детской книги, который жил на Луне.'),
    new Question('Какая книга является самой маленькой в мире?', 'Хамелеон', 'Название совпадает с хорошо маскирующимся животным.'),
    new Question('Где находятся острова, которые были выдуманы Джонатаном Свифтом для произведения «Путешествия Гулливера»?', 'Тихий океан', 'Самый большой океан в мире.'),
    new Question('Сколько лет было Анне Карениной на момент ее гибели в произведении Л.Н.Толстого?', '28', 'Диапазон от 20 до 30.'),
    new Question('В Древней Греции на этих зданиях было написано: «Здесь живут мертвые и говорят живые».', 'Библиотеки', 'Место, где можно брать книги на время.'),
  ];

  getQuestions() {
    return this.questionsArray.slice();
  }

  changeStatus(answer: string, index: number) {
    if(answer === this.questionsArray[index].correctAnswer) {
      this.questionsArray[index].status = 'Ответ верный';
    } else if (answer === '') {
      return;
    } else if (answer !== this.questionsArray[index].correctAnswer) {
      this.questionsArray[index].status = 'Ответ неверный';
    }
    this.questionsArrayChange.emit(this.questionsArray);
    return this.questionsArray[index].status;
  }
}
