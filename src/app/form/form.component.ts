import { Component, Input, OnInit } from '@angular/core';
import { QuestionService } from '../shared/question.service';
import { Question } from '../shared/question.model';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @Input() index = 0;
  questionsArray: Question[] = [];
  answer = '';
  status = '';

  constructor(private questionService: QuestionService) {}

  ngOnInit() {
    this.questionsArray = this.questionService.getQuestions();
    this.questionService.questionsArrayChange.subscribe((question: Question[]) => {
      this.questionsArray = question;
    });
  }

  getUserAnswer() {
    this.questionService.changeStatus(this.answer, this.index);
    this.status = this.questionsArray[this.index].status;
  }
}
